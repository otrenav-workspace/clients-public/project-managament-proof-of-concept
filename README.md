
| [Website](http://links.otrenav.com/website) | [Twitter](http://links.otrenav.com/twitter) | [LinkedIn](http://links.otrenav.com/linkedin)  | [GitHub](http://links.otrenav.com/github) | [GitLab](http://links.otrenav.com/gitlab) | [CodeMentor](http://links.otrenav.com/codementor) |

---

# Project management app

- Omar Trejo
- February, 2017

The client wanted to develop a quick, basic, proof-of-concept project management
app. The code in this repository has been obfuscated to protect the client's
identity. The code is unfinished (the client wanted to further work on this on
his own), and is not completely clean (there are various parts, specifically in
Angular, that could be removed without removing functionality). There are no
tests and there various corner cases that have not been taken care of.

## Stack

- Django (backend)
- Angular 2 (frontend)
- Backend and frontend are separated
- Communicatino with Django REST API

---

> "The best ideas are common property."
>
> —Seneca
